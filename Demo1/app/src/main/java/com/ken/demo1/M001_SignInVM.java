package com.ken.demo1;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class M001_SignInVM extends BaseObservable {
    private  String email;
    private String password;

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    public void onClickSignIn(){

    }
}
