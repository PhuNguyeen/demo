package com.ken.demo1;

import android.text.TextUtils;
import android.util.Patterns;

public class User {
    private int id;
    private String name;
    private String email;
    private String passWord;

    public User() {

    }

    public User(int id, String name, String email, String passWord) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.passWord = passWord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public boolean isValEmail() {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isValPassword() {
        return !TextUtils.isEmpty(passWord) && passWord.length() >= 6;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof User)) return false;
//        User user = (User) o;
//        return Objects.equals(getEmail(), user.getEmail()) && Objects.equals(getPassWord(), user.getPassWord());
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(getEmail(), getPassWord());
//    }
}
